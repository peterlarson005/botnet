#!/bin/bash

sleep 30

expect /attack.exp &

sleep 5

curl -o /dev/null -s -w '%{time_total}\n' http://$webserverip >> /results.txt
PRECALC="$(cat /results.txt | awk 'NR==1{print $1}')"
POSTCALC="$(cat /results.txt | awk 'NR==2{print $1}')"

PRE=$(echo $PRECALC | sed "s/[.]//g")
POST=$(echo $POSTCALC | sed "s/[.]//g")
echo "Pre-Attack Time: "$PRE
echo "Post-Attack Time: "$POST

if [ $PRE -lt $POST ]
then
	#POST-ATTACK GREATER REPONSE TIMES
	echo "Attack SUCCESS"
	exit 0
else
	#POST-ATTACK LESS RESPONSE TIME
	"Attack FAILED"
	exit 1
fi

exit 0


